using Glob
using Base.Filesystem
# concat all estimated gene trees
for d in [abspath(i) for i in glob("*-500/R*")]
    # println(`cat */*/RAxML_bipartitions.final.f200`)
    cd(d)
    files = glob("*/*/RAxML_bipartitions.final.f200")
    run(pipeline(`cat $files`, stdout="gtree_est.trees"))
end

