using Glob
using Base.Filesystem
# concat all true gene trees
for d in [abspath(i) for i in glob("*-true/R*")]
    # println(`cat */*/RAxML_bipartitions.final.f200`)
    cd(d)
    files = glob("*/true.gt")
    run(pipeline(`cat $files`, stdout="gtree_true.trees"))
end

